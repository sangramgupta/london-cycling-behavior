import apache_beam as beam
from apache_beam.options.pipeline_options import PipelineOptions, GoogleCloudOptions, StandardOptions, SetupOptions
from apache_beam.io.gcp.bigquery import ReadFromBigQuery
from apache_beam.io import WriteToText
import logging
import os
import argparse


class ComputeRideCounts(beam.DoFn):
    """
    A DoFn to compute ride counts.

    This class processes each ride record and yields a key-value pair
    with the start and end station IDs as the key, and 1 as the value.
    """

    def process(self, element):
        # For each ride record, yield a key-value pair ((start_station_id, end_station_id), 1)
        yield ((element['start_station_id'], element['end_station_id']), 1)


class ComputeDistances(beam.DoFn):
    """
    A DoFn to compute distances between stations.

    This class processes each ride count record, looks up the station
    locations, and yields the start station ID, end station ID, count
    of rides, and the total distance between stations.
    """

    def process(self, element, station_locations_dict):
        from geopy.distance import geodesic
        (start_station_id, end_station_id), count = element
        logging.info(f'start_station_id: {start_station_id}, end_station_id: {end_station_id}')
        start_location = station_locations_dict.get(start_station_id)
        end_location = station_locations_dict.get(end_station_id)

        if start_location and end_location:
            logging.info(f'start_location: {start_location}, end_location: {end_location}')
            # Calculate the total distance by multiplying the geodesic distance by the count of rides
            distance = geodesic(start_location, end_location).km * count
            logging.info(f'distance: {distance}')
            yield (start_station_id, end_station_id, count, distance)
        else:
            logging.warning(f'Skipping record due to missing location(s): {element}')



def run(argv=None):
    """
    Runs the Apache Beam pipeline.

    This function sets up and runs the pipeline with the specified
    options and arguments.
    """
    parser = argparse.ArgumentParser()
    parser.add_argument(
        '--project',
        required=True,
        help='GCP Project ID')
    parser.add_argument(
        '--region',
        required=True,
        help='GCP Region')
    parser.add_argument(
        '--staging_location',
        required=True,
        help='GCS staging location')
    parser.add_argument(
        '--temp_location',
        required=True,
        help='GCS temp location')
    parser.add_argument(
        '--output_path',
        required=True,
        help='GCS output path')
    known_args, pipeline_args = parser.parse_known_args(argv)

    pipeline_options = PipelineOptions(pipeline_args)
    gcp_options = pipeline_options.view_as(GoogleCloudOptions)
    gcp_options.project = known_args.project
    gcp_options.region = known_args.region
    gcp_options.staging_location = known_args.staging_location
    gcp_options.temp_location = known_args.temp_location
    gcp_options.job_name = 'london-bicycles'
    pipeline_options.view_as(StandardOptions).runner = 'DataflowRunner'

    setup_file_path = os.path.abspath(os.path.join(os.path.dirname(__file__), 'setup.py'))
    pipeline_options.view_as(SetupOptions).setup_file = setup_file_path

    with beam.Pipeline(options=pipeline_options) as p:
        # Read stations data from BigQuery
        stations = p | 'ReadStations' >> ReadFromBigQuery(
            query='SELECT id station_id, latitude, longitude FROM `bigquery-public-data.london_bicycles.cycle_stations`',
            use_standard_sql=True
        )

        station_locations = stations | 'MapStationLocations' >> beam.Map(
            lambda x: (int(x['station_id']), (float(x['latitude']), float(x['longitude']))))

        # Log each mapped station location
        station_locations | 'LogMappedStationLocations' >> beam.Map(lambda x: logging.info(f'Mapped Station: {x}'))

        # Convert to dictionary for distance calculation
        station_locations_dict = beam.pvalue.AsDict(station_locations)

        # Read rides data from BigQuery
        rides = p | 'ReadRides' >> ReadFromBigQuery(
            query='SELECT start_station_id, end_station_id FROM `bigquery-public-data.london_bicycles.cycle_hire`',
            use_standard_sql=True
        )

        # Easy Test: Calculate ride counts
        ride_counts = (
                rides
                | 'PairRides' >> beam.ParDo(ComputeRideCounts())
                | 'CountRides' >> beam.combiners.Count.PerKey()
        )

        # Format ride counts and write to text with header
        ride_counts | 'FormatCounts' >> beam.Map(
            lambda x: f"{x[0][0]},{x[0][1]},{x[1]}") | 'WriteCountsToText' >> WriteToText(
            f'{known_args.output_path}/easy_test', file_name_suffix='.txt',
            header='start_station_id,stop_station_id,amount_of_rides')

        # Explanation of the Formatting String for Easy Test
        # x[0][0]: This extracts the start_station_id from the tuple key.
        # x[0][1]: This extracts the end_station_id from the tuple key.
        # x[1]: This extracts the count of rides (amount_of_rides).
        # This formatting string is used in the Easy Test to create a CSV string with start_station_id, stop_station_id, amount_of_rides.

        # Hard Test: Calculate distances
        ride_distances = (
                ride_counts
                | 'ComputeDistances' >> beam.ParDo(ComputeDistances(), station_locations_dict)
        )

        # Format ride distances and write to text with header
        ride_distances | 'FormatDistances' >> beam.Map(
            lambda x: f"{x[0]},{x[1]},{x[2]},{x[3]:.2f}") | 'WriteDistancesToText' >> WriteToText(
            f'{known_args.output_path}/hard_test', file_name_suffix='.txt')
            # header='start_station_id,stop_station_id,amount_of_rides,total_distance_between_stations')

        # Explanation of the Formatting String for Hard Test
        # x[0]: This extracts the start_station_id.
        # x[1]: This extracts the end_station_id.
        # x[2]: This extracts the count of rides (amount_of_rides).
        # x[3]:.2f: This extracts the total distance between stations and formats it to 2 decimal places.
        # This formatting string is used in the Hard Test to create a CSV string with start_station_id, stop_station_id, amount_of_rides, total_distance_between_stations.


if __name__ == '__main__':
    logging.getLogger().setLevel(logging.INFO)
    run()
