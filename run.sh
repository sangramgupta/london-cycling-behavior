/home/sangram/london-cycle-data/venv/bin/python dataflow-pipeline-modified.py
--project londoncyclinganalysis
--region europe-west1
--staging_location gs://london-cycle-behaviour-analysis/staging
--temp_location gs://london-cycle-behaviour-analysis/temp
--output_path gs://london-cycle-behaviour-analysis/output
