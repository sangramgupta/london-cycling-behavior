# Data Engineer Coding Test

## Objective
In this coding challenge, you will need to perform some data transformations with the public dataset on London bicycles to obtain some insights on London cycling behavior. There are two tables in the London bicycles dataset in BigQuery:
- `bigquery-public-data:london_bicycles.cycle_hire`: cycling hires trip data (1 entry contains 1 trip from one station towards another station)
- `bigquery-public-data:london_bicycles.cycle_stations`: data about the cycling stations (including latitude and longitude information)

To inspect these tables, you will need a GCP project (see instructions).

The goal in this coding challenge is to:

- **Easy test**: Calculate the amount of rides traveled for all possible combinations of start station and end station in the dataset (for example, there are 3 rides from station_id 1 to station_id 2 and 10 rides from station_id 2 to station_id 1).
- **Hard test**: Calculate the total distance covered for all the rides for possible combinations of start station and end station assuming the trip distance between the two stations can be simplified by a straight line between the two station's locations (following the example above, if the distance between station_id 1 and station_id 2 is 2 km, then the distance covered from station_id 1 to station_id 2 is 6 km and 20 km vice versa).

## Important
The idea is that you build a Dataflow pipeline to solve these questions and don’t solve this directly with queries in BigQuery. We don’t want to assess SQL skills but rather the ability to learn new tools in the data engineering space. Therefore, the query responsible for fetching data within your pipeline should be limited to data extraction without applying any transformations. It should utilize only SELECT statements, avoid the use of WHERE clauses or GROUP BY statements in this query. The actual data transformations in your pipeline should be done by using the Apache Beam toolstack.

The coding challenge should approximately take about 4-8 hours.

## Evaluation
The output should be written as text files to Google Cloud Storage under a bucket name of choice and then a subfolder "output". Please make sure there are no other files or directories inside of the folder ‘output’ other than the text files.

- **Easy test**: Each line in these files should be formatted as a string: "start_station_id, stop_station_id, amount_of_rides" (following the example above, output should contain the lines "1,2,3" and "2,1,10"). Your data will be validated by validating the 100 combinations with the highest amount of rides.
- **Hard test**: Each line in these files should be formatted as a string: "start_station_id, stop_station_id, amount_of_rides, total_distance_between_stations" (following the example above, output should contain the lines "1,2,3,6" and "2,1,10,20"). Your data will be validated by validating the 100 combinations with the highest distances. We accept a deviation of 5% on the distance in kilometers. The distance can be submitted either in the form of integers or floats.

The evaluation takes place after submitting the form (see instructions) and you should receive an email within 20 minutes with the outcome. If the answer was not correct, you can retry by submitting the form again. Once you have completed the assignment, we'll invite you for a second interview where you can present the results of your work.

## Instructions
Some instructions to help you get started:

### Set-up
1. Create a free GCP project: https://cloud.google.com/free/
2. Install Google Cloud SDK: https://cloud.google.com/sdk/install
3. Authenticate Google Cloud SDK via command:
    ```sh
    gcloud auth application-default login
    ```
4. Create a Google Cloud Storage bucket: https://cloud.google.com/storage/docs/
5. Create dataflow pipeline
    - Make sure to enable the dataflow API from the GCP console
    - Make sure to set your pipeline options: https://cloud.google.com/dataflow/docs/guides/specifying-exec-params
    - Make sure to set the python pipeline dependencies in a `setup.py` file (also make sure you have the same versions installed locally):
        ```python
        #!/usr/bin/python
        from setuptools import find_packages
        from setuptools import setup

        setup(
            name='Coding-Challenge',
            version='1.0',
            install_requires=[
                'apache-beam[gcp]==2.50.0',
                'geopy==1.18.0',
            ],
            packages=find_packages(exclude=['notebooks']),
            py_modules=['config'],
            include_package_data=True,
            description='Coding Challenge'
        )
        ```

6. Build the pipeline: https://beam.apache.org/documentation/programming-guide/

